import org.kohsuke.github.*;

import java.io.IOException;


public class AccessTest {

    public static void main(String args[]) throws IOException {
        GitHub github = GitHub.connectAnonymously();
        GHRepository a = github.getRepository("emmo-repo/EMMO");

        System.out.println("Branches" + a.getBranches());
        System.out.println("SIZE: " + a.getSize());
        System.out.println("Branch String: " + a.getBranch("refactoring").toString());
        System.out.println("NAME: " + a.getName());
        System.out.println("Description: " + a.getDescription());
        System.out.println("Homepage: " + a.getHomepage());
        System.out.println("Owner: " + a.getOwnerName());
        System.out.println("License: " + a.getLicense());
        //System.out.println("Calloaborators: " + a.getCollaborators());
        System.out.println("OPen issue Count: " + a.getOpenIssueCount());
        System.out.println("ReadMe: " + a.getReadme());
        GHRepositoryStatistics stat = a.getStatistics();
        System.out.println("Stats:1 " + stat.getCodeFrequency());
        //System.out.println("Stats2: " + stat.getContributorStats());

        GHRepositoryStatistics.Participation listP = stat.getParticipation();
        System.out.println("Updated: " +listP.getUpdatedAt());
        System.out.println("Created: " +listP.getCreatedAt());
        System.out.println("Owner commits: " +listP.getAllCommits());

        System.out.println("Stats3: " + stat.getParticipation().getUpdatedAt());
        //System.out.println("Teams: " + a.getTeams());
        //System.out.println("Traffic: " + a.getViewTraffic());
        System.out.println("Watchers: " + a.getWatchersCount());
        System.out.println("Releases: " + a.listReleases().toArray()[0]);


      /*  PagedIterable<GHRepository> listAllPublicRepositories = github.listAllPublicRepositories();
        PagedIterator<GHRepository> iterator = listAllPublicRepositories.iterator();
        while (iterator.hasNext()) {
            GHRepository next = iterator.next();
            System.out.println(next);
        }*/
    }
}
